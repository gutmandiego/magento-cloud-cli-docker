FROM php:7.2

RUN apt update && apt install -y \
    ssh \
    git

COPY entrypoint.sh /entrypoint.sh
COPY installer /installer

RUN chmod +x /entrypoint.sh /installer

WORKDIR /app

ENTRYPOINT ["/entrypoint.sh"]